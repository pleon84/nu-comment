import config from "../ config";
import CommentService from "./services/CommentService.js";
import Http from "./Http.js";

const http = new Http(config.apiBaseUrl);

const api = Object.freeze({
  comment: new CommentService(http),
});

export default api;
