import axios from 'axios';

export default class Http {
  #baseUrl;
  #instance;
  #onError;

  constructor(baseUrl) {
    this.#baseUrl = baseUrl;
  }

  get #http() {
    return this.#instance != null ? this.#instance : this.#initHttp();
  }

  #initHttp() {
    const http = axios.create({
      baseURL: this.#baseUrl,
    });

    http.interceptors.response.use(
      (response) => {
        return response;
      },
      (error) => {
        const { response = {} } = error;
        return this.#handleError(response);
      },
    );

    this.instance = http;
    return http;
  }

  #handleError(error) {
    if (!this.#onError) {
      return Promise.reject(error);
    }

    return this.#onError?.(error);
  }

  get(url, config) {
    return this.#http.get(url, config);
  }

  post(url, data, config) {
    return this.#http.post(url, data, config);
  }
}
