import { makeParamsPagination } from "../../utils/misc.js";

export default class CommentService {
  #http;

  constructor(http) {
    this.#http = http;
  }

  getComment(id) {
    return this.#http.get(`comments/${id}`);
  }

  getList(page, limit, sort) {
    const params = makeParamsPagination(page, limit, sort?.field, sort?.order);
    return this.#http.get(`comments/?${params}`);
  }
}
