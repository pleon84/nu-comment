const config = Object.freeze({
  apiBaseUrl: process.env.baseUrl,
});

export default config;
