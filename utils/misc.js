import { perPage } from "./consts.js";

export function makeParamsPagination(page, limit, sort, order) {
  const params = new URLSearchParams({
    _page: page ?? '1',
    _limit: limit ?? perPage.toString(),
  });

  if (sort) {
    params.append('_sort', sort);
  }

  if (order) {
    params.append('_order', order);
  }

  return params;
}
